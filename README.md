## Evaluation technique back
L'objet de cet exercice est d'évaluer tes connaissances et la qualité du code que tu produis.

Tu ne devrais pas mettre plus de 2h pour compléter les points essentiels. S'il te reste du temps, tu peux évidemment aller plus loin.

Le repo contient une configuration minimale pour un projet typescript. Chaque modification du fichier `tsconfig` devra être motivée. Supprimer des règles pour faciliter la compilation n'est pas une option.

La configuration du projet vise à ce que la compilation se fasse en utilisant les `ES Module`. Les imports devront donc prendre la forme suivante :

```typescript
 import { bar } from './foo.js'
```

Je me permets d'insister sur la présence de l'extension du fichier, si elle n'est pas précisée, tu auras une erreur.

Par ailleurs, concernant l'utilisation de Git, j'aimerais que tu soignes l'historique des commits et que tu découpes bien les étapes de développement (pas de gros commit `done` avec tout l'exercice à la fin).

### Etape 0: Set up
Il faut que tu forks le projet et que tu nommes le nouveau repo avec l'initiale de ton prénom + ton nom (pour Johnny Hallyday, ça sera `jhallyday`). A la fin, de l'exercice, tu devras faire une Merge Request vers le projet initial. Voici [la doc](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) concernant ce workflow

### Etape 1: Créer un serveur Express
Il faut donc que tu crées un serveur Express. Celui-ci doit pouvoir être démarré et arrêté proprement lorsque l'on envoie un signal SIGINT (ctrl + c). Il n'y a pas de déconnexion à une base données à prévoir donc un simple log qui indique que le serveur est bien éteint suffira. 

Un serveur à un cycle de vie donc une approche orientée objet est pertinente mais ce n'est pas une obligation.

### Etape 2: Créer une route GET /user/{id}
L'utilisateur doit pouvoir récupérer un utilisateur du fichier `src/data/users.ts` grâce à une `id` passée en paramètre d'URL.

Une petite gestion d'erreur sera la bienvenue.

### Etape 3: Créer une route POST /validate/user
Une fois de plus, il n'y a pas de base de données, tu ne vas pas pouvoir enregistrer de nouveaux utilisateurs. En revanche, il est toujours possible que tu valides les données du `payload` soumis par un utilisateur.
Celui-ci doit contenir toutes les infos d'un utilisateur sauf son id (cf `/src/data/users.ts`).

Pour ce dernier point, tu es libre d'utiliser la librairie de ton choix.
